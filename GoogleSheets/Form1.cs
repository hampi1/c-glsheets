﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GoogleSheets
{
    public partial class Form1 : MetroFramework.Forms.MetroForm
    {
        public Form1()
        {
            InitializeComponent();
        }
        static string[] Scopes = { SheetsService.Scope.Spreadsheets }; //Режим доступа к таблице
        static string ApplicationName = "GoogleSheets"; //Название приложения


        private string ColumnIndexToColumnLetter(int colIndex) // Великая функция, конвертирующая числовое значение столбца в буквенное (пример3 столбец - C, 1 - A и т.д.)
        {
            int div = colIndex;
            string colLetter = String.Empty;
            int mod = 0;

            while (div > 0)
            {
                mod = (div - 1) % 26;
                colLetter = (char)(65 + mod) + colLetter;
                div = (int)((div - mod) / 26);
            }
            return colLetter;
        }

        private void metroButton1_Click(object sender, EventArgs e) // Обработчик нажатия кнопки Записать данные
        {
            UserCredential credential;
            using (var stream = new FileStream("credentials.json", FileMode.Open, FileAccess.Read))
            {
                string credPath = "token.json";
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
            }
            var service = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            }); //Всё, что выше, предназначено для OAuth2 авторизации

            String range = $"{metroTextBox5.Text}!{ColumnIndexToColumnLetter(Convert.ToInt32(metroTextBox4.Text))}{metroTextBox3.Text}"; // Формирование ссылки на конкретную ячейку (пример Лист1!A2 )
            ValueRange valueRange = new ValueRange
            {
                MajorDimension = "ROWS"
            };
            var oblist = new List<object>() { metroTextBox2.Text }; 
            valueRange.Values = new List<IList<object>> { oblist }; // Получения текта, который необходимо записать в ячейку и связь с ней

            //Всё, что ниже, отвечает за запись изменений в таблицу
            SpreadsheetsResource.ValuesResource.UpdateRequest update = service.Spreadsheets.Values.Update(valueRange, metroTextBox1.Text, range);
            update.ValueInputOption = SpreadsheetsResource.ValuesResource.UpdateRequest.ValueInputOptionEnum.RAW;
            UpdateValuesResponse result2 = update.Execute();
            MessageBox.Show("Великие данные успешно переданы!","Готово!",MessageBoxButtons.OK,MessageBoxIcon.Information);
        }
    }
}
